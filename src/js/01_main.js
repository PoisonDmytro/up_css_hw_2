
let instagramTitle = document.getElementById("instagramShots-titles-first");
let footerParagraph = document.querySelector(".footerContent_paragraph");
let burgerBtn = document.querySelector(".navigation_burgerMenu_btn");
let burgerBtn_cross = document.querySelector(".navigation_burgerMenu_btn_cross");
let burgerMenu =document.querySelector(".navigation_burgerMenu_list")

updateTextBasedOnWindowSize();

function updateTextBasedOnWindowSize() {
    if (window.innerWidth <= 767) {
    instagramTitle.textContent = "- Last Instagram Shot";
    footerParagraph.textContent = "Maecenas faucibus molli interdum. Cras mattis consectetur purus sitor amet sed donec malesuada ullamcorper odio. "
    burgerBtn.style.display = "block";
    } else {
    instagramTitle.textContent = "- Latest Instagram Shots";
    footerParagraph.textContent = "Maecenas faucibus molli interdum. Cras mattis consectetur purus sitor amet sed donec malesuada ullamcorper odio. Curabitur blandit tempus porttitor vollisky inceptos mollisestor."
    burgerBtn.style.display = "none";
    burgerBtn_cross.style.display = "none";
    burgerMenu.style.display = "none";
    }
}

window.addEventListener("resize", updateTextBasedOnWindowSize);


headerId.onclick = function(event) {
    let targetElement = event.target.parentNode.parentNode;
    if(targetElement == burgerBtn) {
        burgerBtn.style.display = "none";
        burgerBtn_cross.style.display = "block";
        burgerMenu.style.display = "flex";
    }
    if(targetElement == burgerBtn_cross) {
        burgerBtn.style.display = "block";
        burgerBtn_cross.style.display = "none";
        burgerMenu.style.display = "none";
    }
}